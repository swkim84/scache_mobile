/* kernel/rwsem.c: R/W semaphores, public implementation
 *
 * Written by David Howells (dhowells@redhat.com).
 * Derived from asm-i386/semaphore.h
 */

#include <linux/types.h>
#include <linux/kernel.h>
#include <linux/sched.h>
#include <linux/export.h>
#include <linux/rwsem.h>

#include <linux/atomic.h>

#ifdef CONFIG_SCACHE
#include <linux/scache.h>
#endif

/*
 * lock for reading
 */
void __sched down_read(struct rw_semaphore *sem)
{
#ifdef CONFIG_SCACHE
	bool is_critical_task;

	spin_lock(&sem->write_owner_lock);
	is_critical_task = scache_set_wait_rwsem(sem);
	if (is_critical_task) {
		if (sem->write_owner && sem->inherit == false) {
			scache_inc_and_set_critical_kernel(sem->write_owner);
			scache_check_waiting_and_inherit(sem->write_owner);
			sem->inherit = true;
		}
		sem->critical_cnt++;
	}
	spin_unlock(&sem->write_owner_lock);
#endif
	might_sleep();
	rwsem_acquire_read(&sem->dep_map, 0, 0, _RET_IP_);

	LOCK_CONTENDED(sem, __down_read_trylock, __down_read);
#ifdef CONFIG_SCACHE
	spin_lock(&sem->write_owner_lock);
	if (is_critical_task)
		sem->critical_cnt--;
	else if (current->waiting == SCACHE_WAITING_NONE)
		sem->critical_cnt--;
	WARN_ON(sem->critical_cnt < 0);
	spin_unlock(&sem->write_owner_lock);

	scache_clear_wait(current);
#endif
}

EXPORT_SYMBOL(down_read);

/*
 * trylock for reading -- returns 1 if successful, 0 if contention
 */
int down_read_trylock(struct rw_semaphore *sem)
{
	int ret = __down_read_trylock(sem);

	if (ret == 1)
		rwsem_acquire_read(&sem->dep_map, 0, 1, _RET_IP_);
	return ret;
}

EXPORT_SYMBOL(down_read_trylock);

/*
 * lock for writing
 */
void __sched down_write(struct rw_semaphore *sem)
{
	might_sleep();
	rwsem_acquire(&sem->dep_map, 0, 0, _RET_IP_);

	LOCK_CONTENDED(sem, __down_write_trylock, __down_write);
#ifdef CONFIG_SCACHE
	spin_lock(&sem->write_owner_lock);
	if (sem->critical_cnt > 0) {
		scache_inc_and_set_critical_kernel(current);
		sem->inherit = true;
	} else
		sem->write_owner = current;
	spin_unlock(&sem->write_owner_lock);
#endif
}

EXPORT_SYMBOL(down_write);

/*
 * trylock for writing -- returns 1 if successful, 0 if contention
 */
int down_write_trylock(struct rw_semaphore *sem)
{
	int ret = __down_write_trylock(sem);

	if (ret == 1)
		rwsem_acquire(&sem->dep_map, 0, 1, _RET_IP_);
#ifdef CONFIG_SCACHE
	if (ret == 1) {
		spin_lock(&sem->write_owner_lock);
		if (sem->critical_cnt > 0) {
			scache_inc_and_set_critical_kernel(current);
			sem->inherit = true;
		} else
			sem->write_owner = current;
		spin_unlock(&sem->write_owner_lock);
	}
#endif
	return ret;
}

EXPORT_SYMBOL(down_write_trylock);

/*
 * release a read lock
 */
void up_read(struct rw_semaphore *sem)
{
	rwsem_release(&sem->dep_map, 1, _RET_IP_);

	__up_read(sem);
}

EXPORT_SYMBOL(up_read);

/*
 * release a write lock
 */
void up_write(struct rw_semaphore *sem)
{
#ifdef CONFIG_SCACHE
	spin_lock(&sem->write_owner_lock);
	sem->write_owner = NULL;
	if (sem->inherit) {
		scache_dec_and_clear_critical_kernel(current);
		sem->inherit = false;
	}
	spin_unlock(&sem->write_owner_lock);
#endif
	rwsem_release(&sem->dep_map, 1, _RET_IP_);

	__up_write(sem);
}

EXPORT_SYMBOL(up_write);

/*
 * downgrade write lock to read lock
 */
void downgrade_write(struct rw_semaphore *sem)
{
#ifdef CONFIG_SCACHE
	spin_lock(&sem->write_owner_lock);
	sem->write_owner = NULL;
	if (sem->inherit) {
		scache_dec_and_clear_critical_kernel(current);
		sem->inherit = false;
	}
	spin_unlock(&sem->write_owner_lock);
#endif

	/*
	 * lockdep: a downgraded write will live on as a write
	 * dependency.
	 */
	__downgrade_write(sem);
}

EXPORT_SYMBOL(downgrade_write);

#ifdef CONFIG_DEBUG_LOCK_ALLOC

void down_read_nested(struct rw_semaphore *sem, int subclass)
{
	might_sleep();
	rwsem_acquire_read(&sem->dep_map, subclass, 0, _RET_IP_);

	LOCK_CONTENDED(sem, __down_read_trylock, __down_read);
}

EXPORT_SYMBOL(down_read_nested);

void down_write_nested(struct rw_semaphore *sem, int subclass)
{
	might_sleep();
	rwsem_acquire(&sem->dep_map, subclass, 0, _RET_IP_);

	LOCK_CONTENDED(sem, __down_write_trylock, __down_write);
}

EXPORT_SYMBOL(down_write_nested);

#endif


